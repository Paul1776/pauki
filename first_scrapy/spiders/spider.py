#! /usr/bin/env python
# -*- coding: utf-8 -*-

import scrapy
import datetime
# from urlparse import urljoin
# from wildberries.items import WildberriesItem

# Класс получает информацию о товарах интернет-магазина wildberries.ru 
# из категории: "Мужские замшевые ботинки".
# Выходные данные: .json файл.

class WildSpider(scrapy.Spider):
    name = "wild"
    start_urls = [
        'https://www.wildberries.ru/catalog/obuv/muzhskaya/zamshevye-botinki',
    ]


    def parse(self, response):
        for shoes_link in response.xpath('//a[@class="ref_goods_n_p"]/@href').extract():
            # item = WildberriesItem()
            item = {}
            item['timestamp'] = datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
            item['section'] = response.xpath('//span[@itemprop="title"]/text()').extract()
            item['marketing_tags'] = self.get_tags(response)
            # url = response.urljoin(response.url, shoes_link)
            url = shoes_link
            item['url'] = url
            yield scrapy.Request(url, callback=self.parse_shoes, meta={'item': item})

        # Переход на след. страницу пагинации
        next_pages = response.xpath('//div[@class="pager i-pager"]/div/a/@href').extract()
        next_page = next_pages[-1]
        print("********************************************************", next_page, next_pages)
        next_page_url = response.urljoin(next_page)
        yield scrapy.Request(next_page_url, callback=self.parse)


    # Парсинг индивидуалной страницы для каждой обуви
    def parse_shoes(self, response):
        item = response.meta['item']
        item['title'] = response.xpath('//head/title/text()').extract()
        item['rpc'] = response.xpath('//span[@class="j-article"]/text()').extract()
        item['brand'] = response.xpath('//meta[@itemprop="brand"]/@content').extract()
        item['price_data'] = self.get_price(response)
        item['stock'] = self.get_stock(response)
        item['assets'] = self.get_assets(response)
        item['metadata'] = self.get_metadata(response)
        yield item


    def get_tags(self, response):
        return [tag for tag in response.xpath('//a[@class="tag"]/text()').extract()]


    def get_price(self, response):
        price = dict()
        price["current"] = response.xpath('//span[@class="add-discount-text-price j-final-saving"]/text()').extract()
        price["original"] = response.xpath('//del[@class="c-text-base"]/text()').extract()
        price["sale_tag"] = response.xpath('//div[@class="discount-tooltipster-content"]/p/span/text()').extract_first()
        return price

    # Метод для получения информации о наличии товаров
    def get_stock(self, response):
        stock = dict()
        in_stock = dict()
        label_class = response.xpath('//div[@class="j-size-list size-list"]/label/@class').extract()
        sizes = response.xpath('//div[@class="j-size-list size-list"]/label/span/text()').extract()
        i = 0
        for it in label_class:
            if it.find("disabled") > 0:
                in_stock[sizes[i]] = False
            else:
                in_stock[sizes[i]] = True
            i += 1
        stock["in_stock"] = in_stock
        stock["count"] = "no information"
        return stock

    # Метод для парсинга изображений
    def get_assets(self, response):
        assets = dict()
        view360 = dict()
        main_img_link = response.xpath('//a[@class="j-carousel-image enabledZoom"]/@href').extract_first()
        assets["main_image"] = response.urljoin(main_img_link)
        imgs_resp = response.xpath('//a[@class="j-carousel-image enabledZoom"]/@href').extract()
        assets["set_images"] = [response.urljoin(img) for img in imgs_resp]
        assets["view360"] = response.xpath('//a[@class="disabledZoom thumb_3d j-carousel-v360"]/img/@src').extract()
        assets["video"] = response.xpath('//div[@id="container_3d"]/@data-path').extract()
        return assets
    

    def get_metadata(self, response):
        meta = dict()
        meta["__description"] = response.xpath('//meta[@property="og:description"]/@content').extract()
        meta["__name"] = response.xpath('//meta[@itemprop="name"]/@content').extract()
        meta["__image"] = response.xpath('//meta[@itemprop="image"]/@content').extract()
        meta["__brand"] = response.xpath('//meta[@itemprop="brand"]/@content').extract()
        meta["__ratingValue"] = response.xpath('//meta[@itemprop="ratingValue"]/@content').extract()
        meta["__reviewCount"] = response.xpath('//meta[@itemprop="reviewCount"]/@content').extract()
        meta["__url"] = response.xpath('//meta[@itemprop="url"]/@content').extract()
        meta["__price"] = response.xpath('//meta[@itemprop="price"]/@content').extract()
        meta["__priceCurrency"] = response.xpath('//meta[@itemprop="priceCurrency"]/@content').extract()
        return meta