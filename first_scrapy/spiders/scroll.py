# -*- coding: utf-8 -*-
import scrapy, json
from scrapy.http import Request
from scrapy import Selector


class ScrollSpider(scrapy.Spider):
    name = 'scroll'
    page_number = 1
    api_urls = 'http://quotes.toscrape.com/api/quotes?page={0}'
    allowed_domains = ['toscrape.com']
    start_urls = [api_urls.format(page_number)]


    def parse(self, response):
        data = json.loads(response.text)
        print(data)
        for quot in data["quotes"]:
            yield {
                "author_name": quot["author"]["name"],
                "text": quot["text"],
                "tags": quot["tags"]
            }
        if data ["has_next"]:
            print("wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww")
            ScrollSpider.page_number += 1
            print(ScrollSpider.page_number)
            yield scrapy.Request(url=self.api_urls.format(ScrollSpider.page_number), callback=self.parse)

