# -*- coding: utf-8 -*-
import scrapy
from scrapy.http import Request
from scrapy import Selector


class ExampleSpider(scrapy.Spider):
    name = 'avito'
    start_urls = ['https://www.avito.ru/moskva_i_mo?q=%D1%82%D0%B0%D0%BA%D1%81%D0%B8']
    allowed_domains = ['avito.ru']

    def start_requests(self):
        my_headers = {
            "sec-fetch-mode": "navigate",
            "sec-fetch-site": "same-origin",
            "sec-fetch-user": "?1",
            "User-Agent": "Mozilla/5.0 (iPad; CPU OS 11_0 like Mac OS X) AppleWebKit/604.1.34 (KHTML, like Gecko) Version/11.0 Mobile/15A5341f Safari/604.1"
        }
        yield Request(url='https://www.avito.ru/moskva_i_mo?q=%D1%82%D0%B0%D0%BA%D1%81%D0%B8', callback=self.parse, headers=my_headers)

    def parse(self, response):
        # найдем блоки html с информацией
        quotes = response.xpath('//div[contains(@class,  "index-content")]').extract_first()
        # Преобразовываем в Selector
        quotes = [Selector(text=x) for x in quotes]
        for quot in quotes:
        # Ищем ссылки в каждом отдельном объявлении
            link = quot.xpath('//a[@class="snippet-link"]/@href ').extract_first()
            item = {
                'link': link,
                # 'author': author,
                # 'tag': tag
            }
            yield item