# -*- coding: utf-8 -*-
import scrapy
from scrapy.http import Request


class AuthorsSpider(scrapy.Spider):
    name = 'authors'
    start_urls = ['http://quotes.toscrape.com/page/1/']
    allowed_domains = ['toscrape.com']

    def parse(self, response):
        urls = response.xpath('//a[text()="(about)"]/@href').extract()  # Берем все ссылки на авторов
        for j1 in urls:
            j1 = response.urljoin(j1)
            yield scrapy.Request(url=j1, callback=self.parse_details111)  # Запускаем функцию parse_details111 для j1
        # Переходим на следующую страницу
        next_page_url = response.xpath('//a[contains(text(),"Next")]/@href ').extract_first()
        if next_page_url:
            next_page_url = response.urljoin(next_page_url)
            yield scrapy.Request(url=next_page_url, callback=self.parse)    # Запускаем функцию parse для следующей страницы
    def parse_details111(self, response):
        name = response.xpath('//h3/text()').extract()
        birth_day = response.xpath('//p/span[@class="author-born-date"]/text()').extract()
        item = {
            'name': name,
            'birth_day': birth_day,
        }
        yield item