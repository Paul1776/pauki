# -*- coding: utf-8 -*-
import scrapy
from scrapy.http import Request
from scrapy import Selector


class ExampleSpider(scrapy.Spider):
    name = 'example1'
    start_urls = ['http://quotes.toscrape.com/page/1/']
    allowed_domains = ['toscrape.com']

    def start_requests(self):
        my_headers = {
            "sec-fetch-mode": "navigate",
            "sec-fetch-site": "same-origin",
            "sec-fetch-user": "?1"
        }
        yield Request(url='http://quotes.toscrape.com/page/1/', callback=self.parse, headers=my_headers)


    def parse(self, response):
        # найдем блоки html с информацией
        quotes = response.xpath('//div[@class="quote"]').extract()
        # Преобразовываем в Selector
        quotes = [Selector(text=x) for x in quotes]
        for quot in quotes:
            # в каждом таком блоке ищем текс автор и теги
            # text_r = Selector(text=quot).xpath('//span[@class="text"and@itemprop="text"]/text()').extract_first()
            text_r = quot.xpath('//span[@class="text"and@itemprop="text"]/text()').extract_first()
            author = quot.xpath('//small[@class="author"]/text()').extract_first()
            tag = quot.xpath('//a[@class="tag"]/text()').extract()
            item = {
                'text': text_r,
                 'author': author,
                 'tag':tag
            }
            yield item
        # Переходим на следующую страницу
        next_page_url = response.xpath('//a[contains(text(),"Next")]/@href ').extract_first()
        if next_page_url:
            next_page_url = response.urljoin(next_page_url)
            yield scrapy.Request(url=next_page_url, callback=self.parse)




'''
# class Authors(scrapy.Spider):
#     name = 'example2'
#     start_urls = ['http://quotes.toscrape.com/random']
#
#     def parse(self, response):
#         text_a = response.xpath('//small[@class="author"and@itemprop="author"]/text()').extract()
#         print(text_a)
#         yield {'text': text_a}
#
#
#
# class AuthorsR(scrapy.Spider):
#     name = 'example3'
#     # start_urls = ['http://quotes.toscrape.com/random']
#
#     def start_requests(self):
#         yield Request(url='https://www.wildberries.ru/catalog/obuv/zhenskaya/sabo-i-myuli/myuli', callback=self.parseyyy)
#
#
#     def parseyyy(self, response):
#         text_b = response.xpath('//a[@class="next"]/@href').extract()
#         print(text_b)
#         list_prod_urls = response.xpath('//a[@class="ref_goods_n_p"]/@href').extract()
#
#         result = {
#             "text": text_b[0],
#             "llpu": list_prod_urls
#          }
#         yield result
#         url = "https://www.wildberries.ru" + text_b[0]
#         yield Request(url, callback=self.parseyyy)





    # def start_requests2(self):
    #     yield Request(url='', callback=self.parseyyy2)
    #
    #
    # def parseyyy2(self, response):
    #     text_c = response.xpath('//a[@class="ref_goods_n_p"]/@href').extract()
    #     print(text_c)
    #     yield {'text': text_c}
    #     yield Request(url, callback=self.parseyyy2)

'''

