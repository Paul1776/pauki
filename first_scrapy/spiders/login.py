# -*- coding: utf-8 -*-
import scrapy, json
from scrapy import Selector

class LoginSpider(scrapy.Spider):
    name = 'login'
    allowed_domains = ['toscrape.com']
    login_url = ["http://quotes.toscrape.com/login"]
    start_urls = [login_url]

    def parse(self, response):
        page = response.xpath('//form[@action="/login"]').extract
        page = Selector(text=page)
        print("wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww")
        print(page)
        # 1. Извлекаем токен
        token = page.xpath('//form/input[@name="csrf_token"]/@value').extract_first
        # 2. Создаем словарь со значениями формы
        data = {
            "csrf_token": token,
            "username": "abc",
            "password": "abc"
        }
        # 3. Логинемся через post request
        yield scrapy.FormRequest(url=self.login_url, formdata=data, callback=self.parse_quotes())

    def parse_quotes(self, response):
        # найдем блоки html с информацией
        quotes = response.xpath('//div[@class="quote"]').extract()
        # Генератор списка
        quotes = [Selector(text=x) for x in quotes]
        for quot in quotes:
            # в каждом таком блоке ищем текс автор и теги
            # text_r = Selector(text=quot).xpath('//span[@class="text"and@itemprop="text"]/text()').extract_first()
            text_r = quot.xpath('//span[@class="text"and@itemprop="text"]/text()').extract_first()
            author = quot.xpath('//small[@class="author"]/text()').extract_first()
            tag = quot.xpath('//a[@class="tag"]/text()').extract()
            item = {
                'text': text_r,
                'author': author,
                'tag': tag
            }
            yield item
